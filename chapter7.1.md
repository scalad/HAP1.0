# 数据多语言

## 表结构(以sys_role为例)
### 主表
包含除多语言以外的字段，与非多语言的表没有大的差别
```groovy
        createTable(tableName: "sys_role") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(nullable: "false", primaryKey: "true")
            }

            column(name: "code", type: "VARCHAR(30)") {
            }

            column(name: "created_at", type: "DATETIME")

            column(name: "updated_at", type: "DATETIME")

            column(name: "created_by", type: "BIGINT")

            column(name: "updated_by", type: "BIGINT")

            column(name: "version", type: "BIGINT")

        }
```
### 多语言表
包含多语言字段，同时还要包含多语言的功能字段：lang,主表id关联字段
```groovy
        createTable(tableName: "sys_role_tl") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(nullable: "false", primaryKey: "true")
            }

            column(name: "role_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "lang", type: "VARCHAR(15)") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(30)") {
                constraints(nullable: "false")
            }

            column(name: "description", type: "VARCHAR(240)") {
            }

        }
```
### 多语言视图
将主表和多语言关联起来
```groovy
        sql(stripComments: true, splitStatements: false, endDelimiter: ';') {
            "CREATE OR REPLACE VIEW sys_role_vl AS SELECT base.*,tl.name,tl.description,tl.id lang_id,tl.lang FROM sys_role base,sys_role_tl tl WHERE tl.role_id=base.id"
        }
```
## Dto
### 多语言注解 

  * MultiLanguage  
    多语主表对应的dto需要添加MultiLanguage，如下：
    ```java
@MultiLanguage(tableName="sys_role_tl",viewName = "sys_role_vl",sequenceName = "sys_role_tl_seq",refColumn = "role_id")
public class Role extends BaseMultiLangDto implements Serializable {
  ...
}
    ```
    
    MultiLanguage 有四个属性：
    * tableName 多语言表名
    * viewName 多语言视图名
    * sequenceName 多语言表ID取值sequence(适用于oracle)
    * refColumn 多语言关联主表id的字段名
  
  * MultiLanguageField
    多语言字段需要添加MultiLanguageField注解
    ```java
@MultiLanguage(tableName="sys_role_tl",viewName = "sys_role_vl",sequenceName = "sys_role_tl_seq",refColumn = "role_id")
public class Role extends BaseMultiLangDto implements Serializable {
    @MultiLanguageField
    private String name;

    @MultiLanguageField
    private String description;
}
    ```
