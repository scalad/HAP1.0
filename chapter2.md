# 新开项目

本章节将引导您从 零 开始，创建一个可以运行的 Demo 工程。此工程基于 hap 标准版，包含一个 demo 实例。
  
## 后端项目

### 确定项目信息
1. groupId  本项目的代号，比如汉得的 BI 产品，代号为 hbi
2. artifactId  本项目的顶层目录名称，使用项目代号(第一个字母大写) + Parent，如 HbiParent
3. package 包名称，使用项目代号 + core ,如 hbi.core
4. version 项目版本，新项目请使用 `1.0-SNAPSHOT`
  
### 新建项目  

确定上述信息后，可以使用如下命令新建项目：
```
mvn archetype:generate -D archetypeGroupId=hap -D archetypeArtifactId=hap-webapp-archetype  -D archetypeVersion=1.0-SNAPSHOT  -D groupId=hbi  -D artifactId=HbiParent  -D package=hbi.core  -D hap.version=1.0-SNAPSHOT  -D archetypeRepository=http://nexus.saas.hand-china.com/content/repositories/rdcsnapshot
```
> 以下内容所指的 `HbiParent` 均指上面命令中的参数 `artifactId` 的值，请按实际情况替换

新的项目目录结构如下：
```
.
├── README.md (项目README，请在此处写上项目开发的注意信息，方便团队协同)
├── core（功能实现项目）
│   ├── pom.xml （子项目core的pom.xml文件）
│   └── src
│       └── main
│           ├── java
│           │   ├── hbi
│           │   │   └── core（前面的包名称）
│           │   │       │   
│           │   │       ├── controllers（Controller包）
│           │   │       │   └── DemoController.java（Controller类）
│           │   │       ├── db（数据表结构，数据初始化入口文件）
│           │   │       │   └── liquibase.groovy
│           │   │       ├── dto（Dto包）
│           │   │       │   └── Demo.java（Dto实现类）
│           │   │       ├── mapper（Mapper包）
│           │   │       │   ├── DemoMapper.java（Mapper接口）
│           │   │       └── service（Service包）
│           │   │           ├── IDemoService.java
│           │   │           └── impl（Service实现）
│           │   │               └── DemoServiceImpl.java
│           │   └── resources（项目配置文件目录）
│           │       ├── mapper
│           │       │   └── DemoMapper.xml（Mapper xml文件）
│           │       ├── spring （spring配置文件目录）
│           │       ├── config.properties
│           │       └── logback.xml（日志配置文件）
│           └── webapp（Webapp目录）
│               ├── lib（UI 资源库目录）
│               └── WEB-INF
│                   ├── web.xml（Web.xml配置）
│                   └── view（页面文件目录）
│                       └── demo（DEMO页面文件目录）
├── core-db（数据库脚本及初始化数据项目）
│   ├── pom.xml
│   └── src
│       └── main
│           └── java
│               └── hbi
│                   └── core
│                       └── db
│                           ├── data（数据文件）
│                           │   └── (init-data)
│                           └── table（数据库表结构管理）
│                               └── 2016-06-01-init-migration.groovy
└── pom.xml

```

### 确定本项目使用的数据库

> 目前已经测试过支持的数据库有Mysql,Oracle,SqlServer  
> 请修改 `HbiParent/core/src/main/java/hbi/core/db/liquibase.groovy` 以适配不同的数据库

确定好数据库后，按照 [Oracle,MySql,Sqlserver数据库配置](chapter1.1.md) 修改项目配置文件。    
修改配置文件后，按照[创建数据库](chapter1.2.md) 中的步骤创建数据库  


### 编译整个项目
在 HbiParent 目录下执行：
```
mvn clean install
```

### 初始化数据库表结构及基础数据
在 HbiParent 项目录下执行：  
```
mvn process-resources -D skipLiquibaseRun=false -D db.driver=com.mysql.jdbc.Driver -D db.url=jdbc:mysql://127.0.0.1:3306/hap_dev -Ddb.user=hap_dev -Ddb.password=hap_dev
```
   - 其中参数按实际情况修改

> 注意  
> 上面生成的项目中已经包含了一个demo功能，请在正式开发前将demo功能的表结构，dto,mapper,service,controller删除。

### 测试
1. 在 HbiParent 工程目录下执行命令(IntelliJ IDEA 用户可以跳过此步骤)
  ```
  mvn eclipse:eclipse
  ```
  
3. 将所有工程导入 IDE 工具（Eclipse，IntelliJ IDEA）中
4. 配置 Server ，配置 JNDI 数据源，参照[多数据库配置](chapter1.1.md)
  - 需要 Tomcat 7+， 不支持 Tomcat 6
5. 将 hbi 工程发布到 tomcat，运行

### 更新框架 HAP 版本

1. 进入本地 maven 仓库 
2. 找到 `com/hand`
3. 删除 `hap`，`hap-db`，`hap-parent`
4. 回到 eclipse ，右键点击 HbiParent 工程，`Maven` → `Update Project...` (等候下载)
5. 右键点击 HbiParent 工程，`Run As` → `Maven build...` → Goals : `clean install`
6. 刷新 HbiParent 相关工程（core，core-db 等）
