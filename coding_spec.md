
# 编码规范
## 1.格式与命名规范
### 1.1	缩进
考虑到代码在所有设备和终端保持同样的排版，代码中不允许使用 Tab，统一采用4个空格作为缩进单位。可以在 IDE 中设置Tab 为4个空格。
### 1.2	换行
每行限制最长120字符，超过120列的行，应按照规范换行。
考虑到如今电脑宽屏为主，120列可以充分利用宽屏空间，也可以避免过多换行。

### 1.3	命名规则
*	不允许使用汉语拼音命名
*	遇到缩写如XML时，仅首字母大写，即loadXmlDocument()而不是loadXMLDocument()
*	Package名必须全部小写，尽量使用单个单词
*	局部变量及输入参数不要与类成员变量同名(get/set方法与构造函数除外)

### 1.4	声明
*	修饰符应该按照如下顺序排列：public, protected, private, abstract, static, final, transient, volatile, synchronized, native, strictfp。
*	类与接口的声明顺序(可用Eclipse的source->sort members功能自动排列):  
  *	静态成员变量 / Static Fields 
  *	静态初始化块 / Static Initializers 
  *	成员变量 / Fields 
  *	初始化块 / Initializers 
  *	构造器 / Constructors 
  *	静态成员方法 / Static Methods 
  *	成员方法 / Methods 
  *	重载自Object的方法如toString(), hashCode() 和main方法
  *	类型(内部类) / Types(Inner Classes) 
  *	同等的类型，按public, protected, private的顺序排列。

## 2	注释规范
### 2.1	注释类型
#### 2.1.1	  Javadoc 注释
Javadoc块的基本格式如下所示：
```java
/**
 * Multiple lines of Javadoc text are written here,
 * wrapped normally…
 */
public int method(String p1){…}
```
或者是以下单行形式：
```java
/** An especially short bit of Javadoc. */
```
基本格式总是OK的。当整个Javadoc块能容纳于一行时(且没有Javadoc标记@XXX)，可以使用单行形式。
#### 2.1.2	失效代码注释
由`/*…*/`界定，标准的C-Style注释，专用于注释已失效的代码。
```java
/*
 *Comment out the code
 * …
 */
 ```
#### 2.1.3	 代码细节注释
由`//`界定，专用于注释代码细节，即使有多行注释也仍然使用`//`，以便于`/**/`区分开来。
	除了私有变量，不推荐使用行末注释。
```java
class MyClass {
    private int myField; // An end-line comment.
    public void myMethod {
//a very very long
       //comment.
       if (condition1) {
//condition1 comment
          ...
        } else {
//elses condition comment
          ...
        }
    }
}
```

### 2.2	注释的格式
*	注释中的第一个句子要以（英文）句号、问号或者感叹号结束。Javadoc生成工具会将注释中的第一个句子放在方法汇总表和索引中。
*	为了在JavaDoc和IDE中能快速链接跳转到相关联的类与方法，尽量多的使用`@see XXx.MyClass，@see XX.MyClass#find(String)`。
*	Class必须以`@author` 作者名声明作者，不需要声明`@version`与`@date`，由版本管理系统保留此信息。
*	如果注释中有超过一个段落，用`<p>`分隔。
*	示例代码以`<pre></pre>`包裹。
*	标识(java keyword, class/method/field/argument名，Constants) 以`<code></code>`包裹。
*	标识在第一次出现时以`{@linkXXx.Myclass}`注解以便JavaDoc与IDE中可以链接。

### 2.3	注释的内容

#### 2.3.1	可精简的注释内容
注释中的每一个单词都要有其不可缺少的意义，注释里不写"@param name -名字"这样的废话。
	如果该注释是废话，连同标签删掉它，而不是自动生成一堆空的标签，如空的@param name，空的`@return`。

#### 2.3.2	推荐的注释内容
*	对于API函数如果存在契约，必须写明它的前置条件(precondition)，后置条件(postcondition)，及不变式(invariant)。
*	对于调用复杂的API尽量提供代码示例。
*	对于已知的Bug需要声明。
*	在本函数中抛出的unchecked exception尽量用@throws说明。

#### 2.3.3	Null规约
如果方法允许null作为参数，或者允许返回值为null，必须在JavaDoc中说明。
	如果没有说明，方法的调用者不允许使用null作为参数，并认为返回值是nullSafe的。
```java    
/**
 * 获取对象.
 *
 * @ return the object to found or null if not found.
 */
Object get(Integer id){
    ...
}
```

#### 2.3.4	特殊代码注释
*	代码质量不好但能正常运行，或者还没有实现的代码用`//TODO`: 或 `//XXX`:声明
*	存在错误隐患的代码用`//FIXME`:声明

## 3	编程规范
### 3.1	基本规范
 1.	当面对不可知的调用者时，方法需要对输入参数进行校验，如不符合抛出IllegalArgumentException，建议使用Spring的Assert系列函数。
 2.	隐藏工具类的构造器，确保只有static方法和变量的类不能被构造实例。
 3.	变量，参数和返回值定义尽量基于接口而不是具体实现类，如`Map map = new HashMap();`
 4.	代码中不能使用`System.out.println()`，`e.printStackTrace()`，必须使用logger打印信息。

### 3.2	异常处理
 1.	重新抛出的异常必须保留原来的异常，即`throw new NewException("message", e);` 而不能写成`throw new NewException("message")`。
 2.	在所有异常被捕获且没有重新抛出的地方必须写日志。  
 3.	如果属于正常异常的空异常处理块必须注释说明原因，否则不允许空的catch块。
 4.	框架尽量捕获低级异常，并封装成高级异常重新抛出，隐藏低级异常的细节。

### 3.3	JDK8
HAP开发以及运行环境统一采用 java 1.8.15（或以上）版本。项目开发人员可使用 java8所提供的特性来是代码更简练。如lambda 表达式等，同时，也要充分利用 java 高版本所带来的便利，如 Annotation 等。

不需要关心的warning信息用`@SuppressWarnings("unused")`, `@SuppressWarnings("unchecked")`, `@SuppressWarnings("serial")` 注释。

