# 目录

* [Introduction](README.md)
* [GitBook使用](chapter0.md)
* [开发环境搭建](chapter1.md)
  * [创建数据库](chapter1.2.md)
  * [Oracle,MySql,Sqlserver数据库配置](chapter1.1.md)

* [新建项目](chapter2.md)
  * [CAS集成](chapter2.1.md)

* [开发流程](chapter3.md)
* [框架介绍](chapter4.md)
  * [消息机制](message.md)  
  * [配置维护](profile.md)  

* 后端开发规范
  * [编码规范](coding_spec.md)
  * [Checkstyle](checkstyle.md)

* [前端开发手册](chapter6.md)
  * [LigerUI 实践](ligerui_in_action.md)
  * [FAQ](faq.md)

* [后端开发](chapter7.md)
  * [数据多语言](chapter7.1.md)
  * [MyBatis 使用手册](chapter7.2.md)
  * [Maven 使用手册](chapter7.3.md)

* [部署](chapter8.md)

