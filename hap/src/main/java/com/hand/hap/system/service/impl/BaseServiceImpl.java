package com.hand.hap.system.service.impl;

import java.util.List;

import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.hand.hap.core.IRequest;
import com.hand.hap.core.annotation.StdWho;
import com.hand.hap.core.exception.UpdateFailedException;
import com.hand.hap.mybatis.common.Mapper;
import com.hand.hap.system.dto.BaseDTO;
import com.hand.hap.system.dto.DTOStatus;
import com.hand.hap.system.service.IBaseService;

/**
 * @author shengyang.zhou@hand-china.com
 */
@Service
public abstract class BaseServiceImpl<T> implements IBaseService<T> {
    @Autowired
    protected Mapper<T> mapper;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<T> select(IRequest request, T condition, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return mapper.select(condition);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public T insert(IRequest request, T record) {
        mapper.insert(record);
        initObjectVersionNumber(record);
        return record;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public T insertSelective(IRequest request, T record) {
        mapper.insertSelective(record);
        initObjectVersionNumber(record);
        return record;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public T updateByPrimaryKey(IRequest request, @StdWho T record) {
        int r = mapper.updateByPrimaryKey(record);
        checkUpdateCount(r, record);
        incObjectVersionNumber(record);
        return record;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public T updateByPrimaryKeySelective(IRequest request, @StdWho T record) {
        int r = mapper.updateByPrimaryKeySelective(record);
        checkUpdateCount(r, record);
        incObjectVersionNumber(record);
        return record;
    }

    /**
     * increment objectVersionNumber after update success.
     * <p>
     * only perform on a BaseDTO.<br/>
     * if original objectVersionNumber is null ,do nothing.
     *
     * @param record
     *            update parameter
     * @return new objectVersionNumber
     */
    protected Long incObjectVersionNumber(Object record) {
        if (record instanceof BaseDTO) {
            BaseDTO bd = (BaseDTO) record;
            if (bd.getObjectVersionNumber() != null) {
                bd.setObjectVersionNumber(bd.getObjectVersionNumber() + 1);
            }
            return bd.getObjectVersionNumber();
        }
        return null;
    }

    /**
     * set objectVersionNumber to 1L.
     * <p>
     * only perform on a BaseDTO.<br/>
     * 
     * @param record
     *            param
     * @return 1L for BaseDTO ,null for others
     */
    protected Long initObjectVersionNumber(Object record) {
        if (record instanceof BaseDTO) {
            ((BaseDTO) record).setObjectVersionNumber(1L);
            return 1L;
        }
        return null;
    }

    /**
     * if updateCount is 0 and record is basedto and objectVersionNumber is NOT
     * null,throw an exception
     * 
     * @param updateCount
     * @param record
     */
    protected void checkUpdateCount(int updateCount, Object record) {
        if (updateCount == 0 && record instanceof BaseDTO) {
            if (((BaseDTO) record).getObjectVersionNumber() != null) {
                throw new RuntimeException(new UpdateFailedException((BaseDTO) record));
            }
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public T selectByPrimaryKey(IRequest request, T record) {
        return mapper.selectByPrimaryKey(record);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(T record) {
        int r = mapper.deleteByPrimaryKey(record);
        checkUpdateCount(r, record);
        return r;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<T> selectAll() {
        return mapper.selectAll();
    }

    /**
     * this method assume the object in list is BaseDTO.
     * 
     * @param request
     *            requestContext
     * @param list
     *            dto list
     * @return the list
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<T> batchUpdate(IRequest request, List<T> list) {
        IBaseService<T> self = ((IBaseService<T>) AopContext.currentProxy());
        for (T t : list) {
            switch (((BaseDTO) t).get__status()) {
            case DTOStatus.ADD:
                self.insertSelective(request, t);
                break;
            case DTOStatus.UPDATE:
                self.updateByPrimaryKeySelective(request, t);
                break;
            case DTOStatus.DELETE:
                self.deleteByPrimaryKey(t);
                break;
            default:
                break;
            }
        }
        return list;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int batchDelete(List<T> list) {
        IBaseService<T> self = ((IBaseService<T>) AopContext.currentProxy());
        int c = 0;
        for (T t : list) {
            c += self.deleteByPrimaryKey(t);
        }
        return c;
    }
}
