# Oracle,MySql,Sqlserver数据库配置


##配置文件

1. 数据源配置
  * hap 默认使用 JNDI 来配置数据源。数据源的配置独立于项目配置
  * 默认使用的 JNDI name 为 `jdbc/hap_dev`
  * tomcat 需要配置 context.xml：
    - Mysql 
    ```xml
    <Resource auth="Container" driverClassName="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/hap_dev" name="jdbc/hap_dev" type="javax.sql.DataSource" username="hap_dev" password="hap_dev"/>
    ```
    - Oracle
    ```xml
    <Resource auth="Container" driverClassName="oracle.jdbc.driver.OracleDriver" name="jdbc/hap_dev" type="javax.sql.DataSource" url="jdbc:oracle:thin:@192.168.115.136:1521:HAP" username="hap_dev" password="hap_dev"/>
    ```
    - SqlServer
    ```xml
    <Resource auth="Container" driverClassName="com.microsoft.sqlserver.jdbc.SQLServerDriver" url="jdbc:sqlserver://10.211.55.6:1433; DatabaseName=hap_dev" name="jdbc/hap_dev" type="javax.sql.DataSource" username="hap" password="handhapdev"/>
    ```
   * 其他应用服务器 weblogic 等，需要按其配置方法 配置 JNDI

2. 多数据库配置
   * hap 项目 根目录 的 pom.xml 中默认包含了 `Oracle`,`Mysql`,`SqlServer` 的 JDBC 驱动
   * 特殊情况下可以修改驱动版本，一般不需要改动
   * 当数据库为 Oracle 时，需要修改  resources/config.properties
     *  
     ```
     mybatis.identity=SEQUENCE
     ```
     * 修改完毕后复制一份放到 resources/profiles/dev (覆盖)
   
3. liquibase配置文件
   * hap-parent/hap/src/java/com/hand/hap/db/liquibase.groovy
     * 新建项目，请修改 {root}/core/src/java/{package}/db/liquibase.groovy
   * 需要修改上面文件中的数据库类型 :`oracle`,`mysql`,`sqlserver`
   





